#!/usr/bin/env python

valorHora = float(raw_input('digite o valor por hora: '))
qtdTrab = int(raw_input('digite a quantidade de horas trabalhadas: '))
totalBruto = valorHora * qtdTrab
ir = 0
if (totalBruto > 2500):
	ir = (totalBruto / 100) * 20
elif (totalBruto > 1501):
	ir = (totalBruto / 100) * 10
elif (totalBruto > 901):
	ir = (totalBruto / 100) * 5

sind = (totalBruto / 100) * 3
fgts = (totalBruto / 100) * 11
inss = (totalBruto / 100) * 10
salLiq = totalBruto - sind - ir


print('Salario bruto:   %.2f' % (totalBruto))
print('IR :             %.2f' % (ir))
print('Sindicato:       %.2f' % (sind))
print('INSS:            %.2f' % (inss))
print('FGTS:            %.2f' % (fgts))
print('Salario Liquido: %.2f' % (salLiq))
