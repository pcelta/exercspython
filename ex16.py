#!/usr/bin/env python

#-*-coding:utf8-*-


import Tkinter
import tkMessageBox

def limpar():
	labelResult.configure(text="")
	entrada1.delete(0,Tkinter.END)
	entrada2.delete(0,Tkinter.END)

def somar():
	ok,n1,n2 = validaEntrada()
	if(ok == True):
		result = n1 + n2
		labelResult.configure(text="Resultado: %.2f" % (result))


def subtrair():
	ok,n1,n2 = validaEntrada()
	if(ok == True):
		result = n1 - n2
		labelResult.configure(text="Resultado: %.2f" % (result))

def multiplicar():
	ok,n1,n2 = validaEntrada()
	if(ok == True):	
		result = n1 * n2
		labelResult.configure(text="Resultado: %.2f" % (result))

def dividir():
	ok,n1,n2 = validaEntrada()
	if(ok == True):	
		if(n2 == 0):
			tkMessageBox.showerror("Erro nos valores","Valor invalido.\nImpossivel dividir por zero")
		else:
			result = n1 / n2
			labelResult.configure(text="Resultado: %.2f" % (result))

def validaEntrada():		
	msg = ""
	numero1 =0
	numero2 =0
	ok = True	
	try:
		numero1 = float(entrada1.get())
	except(Exception,msg):
		tkMessageBox.showerror("Erro nos valores","Valor 1 invalido.\nPor favor digite apenas numeros e separe as casas decimais com ponto")
		ok = False 
	try:
		numero2 = float(entrada2.get())
	except(Exception,msg):
		tkMessageBox.showerror("Erro nos valores","Valor 2 invalido.\nPor favor digite apenas numeros e separe as casas decimais com ponto")
		ok = False		
	

	return ok,numero1,numero2	
		

#
#-----------------------------------------------------------------------------------------------------------------------------
#


# criacao da janela e definicao de titulo

programa = Tkinter.Tk()		
Container1 = Tkinter.Frame(programa)
Container1.pack(expand="true")
Container1.master.title("Calculadora")
		
#		frame das entradas e textBox
	
#frame da entrada 1
frameEntrada1 = Tkinter.Frame(Container1,relief="sunken")	
label1 = Tkinter.Label(frameEntrada1,text="Valor 1")
label1.pack(side="left")
entrada1 = Tkinter.Entry(frameEntrada1)
frameEntrada1.pack(side="top",expand="true")
entrada1.pack(side="left",expand="true")
	
#frame da entrada 2
frameEntrada2 = Tkinter.Frame(Container1)		
label2 = Tkinter.Label(frameEntrada2,text="Valor 2")
label2.pack(side="left")
entrada2 = Tkinter.Entry(frameEntrada2)
frameEntrada2.pack(side="top", expand="true")
entrada2.pack(side="left",expand="true")		


#			BOTOES
                
frameBotoes = Tkinter.Frame(Container1,relief="sunken")

botaoSomar = Tkinter.Button(frameBotoes)
botaoSomar.configure(text="Somar",command =somar)
botaoSomar.pack(side="left",expand="true",padx=5, pady=2)

botaoMultiplicar = Tkinter.Button(frameBotoes)
botaoMultiplicar.configure(text="Multiplicar",command=multiplicar)
botaoMultiplicar.pack(side="left",expand="true",padx=5, pady=2)

botaoDividir = Tkinter.Button(frameBotoes)
botaoDividir.configure(text="Dividir",command=dividir)
botaoDividir.pack(side="left",expand="true",padx=5, pady=2)

botaoSubtrair = Tkinter.Button(frameBotoes)
botaoSubtrair.configure(text="Subtrair",command=subtrair)
botaoSubtrair.pack(side="left",expand="true",padx=5, pady=2)

botaoLimpar = Tkinter.Button(frameBotoes)
botaoLimpar.configure(text="Limpar",command=limpar)
botaoLimpar.pack(side="left",expand="true",padx=5, pady=2)
		
frameBotoes.pack(side="top",expand="true")


#		frame do resultado

frameResult = Tkinter.Frame(Container1)
labelBranco = Tkinter.Label(frameResult,text="")
labelBranco.pack(side="top",expand="true")
labelResult = Tkinter.Label(frameResult,text="")
labelResult.pack(side="left",expand="true")
frameResult.pack(side="top")

#		frame do botao sair

frameSair = Tkinter.Frame(Container1)
labelBranco = Tkinter.Label(frameSair,text="")
labelBranco.pack(side="top",expand="true")
botaoSair = Tkinter.Button(frameSair)
botaoSair.configure(text="Sair", command = Container1.quit)
botaoSair.pack(side="top",expand="true")
frameSair.pack(side="top",expand="true")


programa.mainloop()

