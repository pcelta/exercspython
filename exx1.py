#! /usr/bin/env/python

pe = 3.28
x = 1
while x <= 100:
	metros =  x / pe
	print('%i pe(s) equivale a %f metros' % (x, metros))	
	y = x % 10
	if y == 0:
		print('')
	x = x + 1
