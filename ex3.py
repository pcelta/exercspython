#!/usr/bin/env python

for x in range(1,101):
	if x % 3 == 0:
		if x % 5 == 0:
			print ('%i:%s' % (x,' SpamEgg'))
		else:
			print ('%i:%s' % (x, ' Spam'))
	else: 
		if x % 5 == 0:
			print ('%i:%s' % (x, ' Egg'))
