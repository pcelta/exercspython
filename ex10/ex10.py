#! /usr/bin/env python
# -*- coding: utf-8 -*-

arqEntrada = open('texto.txt', 'r')
arqSaida = open('lista.html', 'w')

lista = arqEntrada.readlines()
lista.sort()
lista.reverse()
listaSaida = ['<html>','<title>','</title>','<body>','<ul>']

for item in lista:
	listaSaida.append('<li>')
	listaSaida.append(item)
	listaSaida.append('</li>')

listaSaida.append('</ul>')
listaSaida.append('</body>')
listaSaida.append('</html>')

arqSaida.writelines(listaSaida)
arqSaida.close()
arqEntrada.close()
