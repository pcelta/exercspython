#!/usr/bin/env python
#-*-coding:utf8-*-

#calculo da area de um circulo

from math import pi

area = 0
raio = 5

area = pi*raio*raio

print("A area de um circulo de raio %i é: %i" % (raio,area))
