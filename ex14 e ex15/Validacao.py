#!/usr/bin/env python
#-*-coding:utf8-*-



#MÓDULO QUE FAZ AS VALIDAÇÕES DE DADOS E RECEBE OS MESMOS VIA TECLADO

def getValida_float(nome_propriedade):
#REBERE UM TIPO FLOAT
	msg = ""
	ok = False
	while(ok == False):
		try:
			valor_float  = float(raw_input("%s: " % (nome_propriedade)))
			ok = True
		except(Exception, msg):
			print("\n%s Inválida(o), digite apenas numeros e separe as casas decimais com ponto\n" %(nome_propriedade))
	return valor_float



def getValida_str(nome_propriedade):
#RECEBE UM TIPO STRING
	msg = ""
	ok = False
	while(ok == False):
		try:
			string= str(raw_input("%s: " % (nome_propriedade))) 	
		except(Exception, msg):
			print("\n%s Inválida(o), digite apenas letras\n" % (nome_propriedade))
		if(string == ""):
			print("\n%s Inválida(o), digite apenas letras\n" % (nome_propriedade))
		else:
			ok = True
	return string



def getValida_int(nome_propriedade):
#RECEBE UM INTEIRO, QUE NO CASO DA APLICAÇÃO SÃO OS ANOS
	msg = ""
	ok = False
	while(ok == False):
		inteiro = True
		try:				
			valor_int  = int(raw_input("%s: " % (nome_propriedade)))
		except(Exception,msg):
			print("\n%s Inválida(o), digite apenas números inteiros entre(1900-2012)\n" %(nome_propriedade))
			inteiro = False

		if(inteiro == True):	
			if(valor_int < 2012 and valor_int > 1900):
				ok = True
			else:
				print("\n%s Inválida(o), digite apenas números inteiros entre(1900-2012)\n" %(nome_propriedade))
		else:
			print("")
	return valor_int


def getValida_opcao(nome_propriedade):
#RECEBE A OPÇÃO DO USUÁRIO E NÃO DEIXA DIGITAR UMA OPÇÃO QUE ESTEJA FORA DO INTERVALO(1-4)
	msg = ""
	ok = False
	while(ok == False):
		try:
			opcao = int(raw_input("\nOpção: "))			
		except(Exception,msg):
			print("\n%s Inválida, digite apenas as opções de 1 a 4!\n" % (nome_propriedade))
			opcao = "fora"
		if((opcao <= 4) and (opcao >= 1)):
			ok = True
			return opcao
		elif(opcao == "fora"):
			print("")
		elif((opcao <= 0) or (opcao > 4)):
			print("\n%s Inválida, digite apenas as opções de 1 a 4!\n" % (nome_propriedade))
		




