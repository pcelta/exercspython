#!/usr/bin/env python
#-*-coding:utf8-*-

from MeuCarro import MeuCarro

from Validacao import getValida_str,getValida_float,getValida_int,getValida_opcao

from Auxiliar import cabecalho,menu_principal,informacoes

class main:
#CLASSE ONDE É EFETUADA TODAS AS CHAMADAS E A EXECUÇÃO DA APLICAÇÃO

	#IMPRESSÃO DO CABEÇALHO
	cabecalho()

	#RECEPÇÃO DE DADOS VIA TECLADO E VALIDAÇÃO
	modelo = getValida_str("Modelo")
	marca =  getValida_str("Marca")
	cor = getValida_str("Cor")	
	ano_fab = getValida_int("Ano de Fabricação")
	ano_mod = getValida_int("Ano do Modelo")
	capacidade_tanque = getValida_float("Capacidade do Tanque")
	qtd_combustivel = getValida_float("Quantidade(Litros) de combustível no tanque")

	#FILTRO PARA IMPEDIR QUE O USUÁRIO DIGITE UMA QUANTIDADE DE COMBUSTIVEL MAIOR QUE A COMPORTADA PELO TANQUE
	while(capacidade_tanque < qtd_combustivel):
		print("A quantidade digita é superior a comportada pelo veículo!\n")
		print("Por favor digite novamente!")
		qtd_combustivel = getValida_float("Quantidade(Litros) de combustível no tanque")


	kmpl = getValida_float("Cosumo em km por litros")	

	#INSTACIANDO UM NOVO MEUCARRO
	novo_carro = MeuCarro(modelo,cor,marca,ano_fab,ano_mod,kmpl,capacidade_tanque,qtd_combustivel)
	
	opcao = 0	
	while(opcao != 4):
		menu_principal()
		opcao = getValida_opcao("Opção")
		if(opcao == 1):
			lista = informacoes(novo_carro)
			for item in lista:
				print(item)
		elif(opcao == 2):
			if(novo_carro.qtd_combustivel == novo_carro.capacidade_tanque):
				print("O veículo já está com o tanque cheio!!\n")
			else:
				print("\nAbastecimento\n")
				qtd_combustivel = getValida_float("Quantidade de litros")
				novo_carro.abastecer(qtd_combustivel)
		elif(opcao == 3):
			if(novo_carro.qtd_combustivel <= 0):
				print("O veículo não tem combustível!")
				print("Abasteça!\n")
			else:
				km_a_percorrer = getValida_float("\nQuantidade de Kilometros a percorrer")
				novo_carro.andar(km_a_percorrer)
		elif(opcao == 4):
			print("\nObrigado por utilizar o Sistema de Autos\n")
		
