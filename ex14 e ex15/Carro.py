#!/usr/bin/env python
#-*-coding:utf8-*-

class Carro:

# SUPER CLASSE


#PROPRIEDADES
	qtd_combustivel=0
	capacidade_tanque=0
	kmpl=0



#CONSTRUTOR, APENAS INICIALIZANDO A PROPRIEDADE CAPACIDADE_TANQUE
	def __init__(self,capacidade_tanque):
		self.capacidade_tanque = capacidade_tanque



	def abastecer(self,qtd_combustivel):
	#METODO QUE ABASTECE O CARRO E RECEBE COMO PARAMETRO A QUANTIDADE DE COMBUSTIVEL A SER ABASTECIDA

		#ATRIBUINDO A SOMA DO COMBUSTIVEL JA EXISTENTE NO TANQUE COM A QUANTIDADE A SER ABASTECIDA
		total = qtd_combustivel + self.qtd_combustivel
		print("\nCarro abastecido!!\n")
		if(total > self.capacidade_tanque ):
			#SE A VARIAVEL TOTAL FOR MAIOR QUE A CAPACIDADE DO TANQUE( 
			#ISTO QUER DIZER QUE A QUANTIDADE A SER ABASTECIDA É MAIOR QUE A COMPORTADA PELO VEÍCULO),
			#FAÇA AS INTRUÇÕES A SEGUIR
			print("Abastecimento acima do comportado pelo veículo\n")	
			print("Sobrou: %.2f litro(s)\n" % (total - self.capacidade_tanque))
			self.qtd_combustivel = self.capacidade_tanque
		else:
			self.qtd_combustivel = total




	def andar(self,km_a_percorrer):
	#METODO QUE SIMULA O CONSUMO DE COMBUSTIVEL E RECEBE COMO PARAMETRO A QUANTIDADE DE KILOMETROS A PERCORRER 
		combustivel_gasto = self.qtd_combustivel
		km = km_a_percorrer
		#A VARIAVEL KM SERÁ UTILIZADA PARA VERIFICAR SE O CARRO SE DESLOCOU
		ok = False
		while(ok == False):
			
			if(self.qtd_combustivel > 0):
			#SE EXISTIR COMBUSTIVEL NO TANQUE
				if(self.qtd_combustivel < 1):
					km_a_percorrer-= self.qtd_combustivel * self.kmpl
					self.qtd_combustivel = 0
				if(km_a_percorrer > 0):
				#SE EXISTIR KM A PERCORRER
					if(km_a_percorrer >= self.kmpl):
					#SE KM A PERCORRER FOR MAIOR OU IGUAL AO CONSUMO
						km_a_percorrer = km_a_percorrer - self.kmpl
						self.qtd_combustivel -= 1
						#NESTE TRECHO É CONTABILIZADO PARTE DO CONSUMO
					else:					
						#SE O KM A PERCORRER FOR MENOR AO KM POR LITRO ENTÃO
						#O VEICULO ANDARÁ MENOS DE UM KILOMETRO E SERÁ DIVIDIDO O
						#O KM A PERCORRER PELO KM/POR LITRO, PARA CONTABILIZAR O CONSUMO
						
						self.qtd_combustivel = self.qtd_combustivel - (km_a_percorrer / self.kmpl)
						ok == True
						break
				else:
				#SE O KM A PERCORRER FOR MENOR OU IGUAL A ZERO
					combustivel_gasto -= self.qtd_combustivel
					print("Combustivel gasto na viagem %.2fL" % (combustivel_gasto))
					ok = True
			else:
			#SE A QUANTIDADE DE COMBUSTIVEL FOR IGUAL A ZERO OU MENOR
				
				if(km != km_a_percorrer):
				#SE A VARIAVEL KM(ONDE FOI ARMAZENADA O KM A PERCORRER ORIGINAL) FOR DIFERENTE DA VARIAVEL
				#KM_A_PERCORRER(QUE FOI UTILIZADA NOS TRECHOS ACIMA E SUBTRAÍDA CASO O CARRO TENHA SE DESLOCADO)
					km = km - km_a_percorrer
					#A VARIAVEL KM ARMAZENA QUANTIDADE PERCORRIDA EM CASO DE FALTA DE COMBUSTIVEL
					if(km_a_percorrer == 0):
						#CASO O VEICULO TENHA PERCORRIDO TODO  O PERCURSO IRÁ APARECER APENAS A MSG ABAIXO
						print("Combustivel gasto na viagem %.2fL" % (combustivel_gasto))
					else:
						#CASO O VEICULO TENHA PARADO POR FALTA DE COMBUSTIVEL E NÃO TENHA COMPLETADO O
						#PERCURSO IRÁ APARECER A MSG ABAIXO
						print("O veículo parou por falta de combustível!!")
						print("Foi percorrido %.2f kilometros\n" % (km))
				else:				
					#CASO O USUÁRIO ESCOLHA A OPÇÃO DE ANDAR, MAS O VEICULO NÃO TENHA COMBUSTIVEL APARECERÁ
					# A AMSG ABAIXO
					print("O veículo não tem combustível!")
					print("Abasteça!\n")
				ok = True




