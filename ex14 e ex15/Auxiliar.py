#!/usr/bin/env python
#-*-coding:utf8-*-


#MÓDULO COM AS FUNÇÕES DE EXIBIÇÃO DE CABEÇALHO,MENU E INFORMAÇÕES DO VEÍCULO

def cabecalho():
	print("\n********************************\n")
	print("***Digite os dados do Veículo***\n")
	print("********************************\n")

def menu_principal():
	print("\n")
	print("* * * * * * * Sistema de Autos * * * * * * *\n")
	print("Escolha uma das opções\n")
	print("1 - Informações sobre o Veiculo")
	print("2 - Abastecer")
	print("3 - Andar")
	print("4 - SAIR\n")
	print("* * * * * * * * * * * * * * * * * * * * * * *\n")

def informacoes(MeuCarro):
#FUNÇÃO QUE RECEBE COMO PARAMETRO UM OBJETO DO TIPO MEUCARRO E RETORNA UMA LISTA COM FORMATADA COM AS INFORMAÇÕES DO VEÍCULO
	lista = [""]
	asteriscos = ""
	cabecalho = "Dados do Veículo"
	lista.append("")
	lista.append(cabecalho.ljust(55))
	lista.append("")
	lista.append("Modelo:".ljust(30) + MeuCarro.modelo.ljust(25))
	lista.append("Cor:".ljust(30) + MeuCarro.cor.ljust(25))
	lista.append("Marca:".ljust(30) + MeuCarro.marca.ljust(25))
	
	
	#ARMAZENANDO AS PROPRIEDADES DE MEUCARRO QUE SÃO DIFERENTES DE STRING EM OUTRAS VARIAVEIS E
	# JÁ TRANSFORMADAS PARA STRING
	#PARA POSSIBILITAR A FORMATAÇÃO NA LISTA	
	fab = "%i" % (MeuCarro.ano_fab)
	mod = "%i" % (MeuCarro.ano_mod)
	capacidade = "%.1f" % (MeuCarro.capacidade_tanque)
	qtd_comb = "%.1f" % (MeuCarro.qtd_combustivel)
	kmpl = "%.1f" % (MeuCarro.kmpl)

	lista.append("Ano de Fabricação:".ljust(32) + fab.ljust(25))	
	lista.append("Ano do Modelo:".ljust(30) + mod.ljust(25))	
	lista.append("Capacidade do Tanque:".ljust(30) + (capacidade+"L").ljust(25))	
	lista.append("Quantidade de Combustivel:".ljust(30) + (qtd_comb+"L").ljust(25))	
	lista.append("Consumo por Litro:".ljust(30) + (kmpl+"km/L").ljust(25))	
	lista.append("")
	
	
	return lista
	


