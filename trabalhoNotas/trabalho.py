#!/usr/bin/env python

arquivo = open('arquivo.txt','w')
mediaGeral = 0
linha = '-' * 75
cabecalho = ('Aluno        Nota 1     Nota 2     Nota 3     Nota 4     Media     Situacao\n')
listaSaida = [cabecalho.ljust(76)]        
listaSaida.append(linha.ljust(76))
for x in range(0,10):
	nome = str(raw_input('Digite o nome do Aluno: '))
	n1 = float(raw_input('Digite a nota 1: '))
	n2 = float(raw_input('Digite a nota 2: '))
	n3 = float(raw_input('Digite a nota 3: '))
	n4 = float(raw_input('Digite a nota 4: '))
	mediaAluno = (n1 + n2 + n3 + n4) / 4
	if (mediaAluno >= 6):
		situacao = 'Aprov.'
	else:
		situacao = 'Reprov.'
	mediaGeral = mediaGeral + mediaAluno
	nome = '\n%s' % (nome)
	stringN1 = '%.2f' % (n1)
	stringN2 = '%.2f' % (n2)
	stringN3 = '%.2f' % (n3)
	stringN4 = '%.2f' % (n4)
	stringMediaA = '%.2f' % (mediaAluno)
	listaSaida.append(nome.ljust(14))
	listaSaida.append(stringN1.ljust(11))
	listaSaida.append(stringN2.ljust(11))
	listaSaida.append(stringN3.ljust(11))
	listaSaida.append(stringN4.ljust(11))
	listaSaida.append(stringMediaA.ljust(10))
	listaSaida.append(situacao.center(8))
listaSaida.append('\n\n')
unders = '-' * 9	
mediaGeral =  mediaGeral / 10
stringMediaG = 'Media Geral:  %.2f' % (mediaGeral)
listaSaida.append(unders.center(75))
listaSaida.append('\n')
listaSaida.append(stringMediaG.rjust(75))
arquivo.writelines(listaSaida)
arquivo.close()

